let checkBox = document.querySelector("input[name=checkBox]");


checkBox.addEventListener('change', (e) => {
    e.preventDefault();
    
    let adminIsActive = localStorage.getItem("isActive");
    let token = localStorage.getItem('token');

    const urlParams = new URLSearchParams(window.location.search);
    const courseId = urlParams.get('courseId');

     fetch(`http://localhost:4000/api/courses/${courseId}`, { 
	 method: 'DELETE',
      headers: {
	  'Content-Type': 'application/json',
	   'Authorization': `Bearer ${token}`

		},
	  		body: JSON.stringify({
     	    courseId: courseId,	
     	    updates: adminIsActive
   		    
  
   		})

	})
	  
	  .then(res => {
   	  return res.json()
   })
   .then(data => {
   	//creation of new course successful
   	if(data === true){
   		//redirect to course page

   		alert('Updated!')
   		window.location.replace("./course.html")
   	}else{
   		//redirect in creating course
   		alert('Something went wrong')
   	 }

    })
})

