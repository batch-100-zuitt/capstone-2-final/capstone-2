let formSubmit2 = document.querySelector('#editCourse');




formSubmit2.addEventListener('submit', (e) => {
	e.preventDefault()


	let firstname = document.querySelector('#firstname').value
	let lastname = document.querySelector('#lastname').value
	let email = document.querySelector('#userEmail').value

	let token = localStorage.getItem('token');
	
	const urlParams = new URLSearchParams(window.location.search);
    const userId = urlParams.get('userId');
    

	 fetch('http://localhost:4000/api/users/editProfile', { 
	 method: 'PUT',
      headers: {
	  'Content-Type': 'application/json',
	   'Authorization': `Bearer ${token}`

	},
	  	body: JSON.stringify({
        userId: userId,	
   		firstName: firstname,
   		lastName: lastname,
   		email: email 
  
   })

})
	  
	  .then(res => {
   	  return res.json()
   })
   .then(data => {
   	//creation of new course successful
   	if(data === true){
   		//redirect to course page

   		alert('Updated!')
   		window.location.replace("./profile.html")
   	}else{
   		//redirect in creating course
   		alert('Something went wrong')
   	 }

    })

})
  