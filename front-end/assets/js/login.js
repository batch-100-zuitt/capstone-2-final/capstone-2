  let loginForm = document.querySelector("#logInUser");

//add an event listener to the form
 //(e) meaning when it is triggered, it will create an event object
  loginForm.addEventListener("submit", (e) => {
  	e.preventDefault();

    let    email = document.querySelector("#userEmail").value;
    let password = document.querySelector("#password").value;



   if(email == "" && password == "") {
	  alert("Please input your email and  password.");

    } else if(email == "") {
      alert("Please input Email");

    } 
    else if(password == "") {
      alert("Please input Password");

    }else {
    	fetch('http://localhost:4000/api/users/login', {
    		method: 'POST',
    		headers: {
               'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			email: email,
    			password: password
    		})
    	})
    	.then(res => {
    		return res.json()
    	})
    	.then(data => { console.log(data)
            if(data.accessToken){
            	//store JWT on local storage
            	localStorage.setItem('token',data.accessToken);
            	//send fetch request to decode JWT and obtain user ID role for storing in context
            	fetch('http://localhost:4000/api/users/details',{
                     headers:{
                     	Authorization: `Bearer ${data.accessToken}`
                     }
               
            	})  
                .then(res => {
                   return res.json();

          	     })  

            	.then(data => {                   
            		//set the global user to have properties containing authenticated user's IS and role
            		localStorage.setItem("id", data._id)
            		localStorage.setItem("isAdmin", data.isAdmin)
                localStorage.setItem("firstName", data.firstName)
                localStorage.setItem("lastName", data.lastName)

                //I first set get the isAdmin in my localstorage that was stored
                let adminUser = localStorage.getItem("isAdmin");
                //condition for user if it's admin or not, the user will go to different pages  
                if( adminUser == "false" || !adminUser){
            		window.location.replace("./../indexUser.html")
                }
                else{
                    window.location.replace("./../indexAdmin.html")
                }
            	})
            } else {
            	//authentication failure
            	alert('Wrong email or password')
            }
    	})
    }

})

