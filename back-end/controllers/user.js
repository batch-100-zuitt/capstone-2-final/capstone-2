const User = require('../models/user')
const Course = require('../models/course')
const bcrypt = require('bcryptjs')
const auth = require('../auth')

//for checking if email has a duplicate
module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

// for register
module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

//for user log-in
module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { return false }

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
	})
}



//for editing profile
module.exports.editProfile = (params) => {
	let editedProfile = {

	 firstName: params.firstName,
	  lastName: params.lastName,
	     email: params.email

	}

	return User.findByIdAndUpdate(params.userId, editedProfile)
	.then((profile,err) => {
    	if (err){
    		console.log(err)
    }
     return (err) ? false:true
	})

}



// for finding a specific id and returning it's details
module.exports.get = (params) => {	
	return User.findById(params.userId)
	.then(user => {
		user.password = undefined
		return user
   	})	
   
}


//for getting all users that is not admin
module.exports.getAllUsers = () => {
	return User.find({ isAdmin: false }).then(user => user)
   

}


//for enrolling
module.exports.enroll = (params) => {
	return User.findById(params.userId)
	.then(user => {
		user.enrollments.push({ courseId: params.courseId })

		return user.save()
		.then((user, err) => {
			return Course.findById(params.courseId)
			.then(course => {
				course.enrollees.push({ userId: params.userId })

				return course.save().then((course, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}

module.exports.updateDetails = (params) => {
	
	
}

module.exports.changePassword = (params) => {
	
}

module.exports.verifyGoogleTokenId = (params) => {
	
}