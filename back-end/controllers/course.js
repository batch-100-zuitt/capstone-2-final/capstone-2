const Course = require('../models/course')


//for getting All courses
module.exports.getAll = () => {			
	return Course.find().then(courses => courses)
}


//for getting all active courses
module.exports.getAllActive = () =>{
  return Course.find({ isActive: true }).then(courses => courses)
}

// for adding new course
module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

//for getting details of the courseId
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

//for updating course
module.exports.update = (params) => {
	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		if (err){
			console.log(err)
		}
		return (err) ? false : true
	})
}


//for deactivating course
module.exports.archive = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}




