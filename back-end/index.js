const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

app.use(cors())


mongoose.connect('mongodb+srv://admin:graplerchina213@wdc028-course-booking.yxol4.mongodb.net/restbookingtest?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})